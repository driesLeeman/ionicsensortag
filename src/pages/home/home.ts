import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BLE } from '@ionic-native/ble';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {



  constructor(public navCtrl: NavController,private ble: BLE) {
    

  }
  private bluetoothdevice : string;
  private temperature : string
  private airPressure : string
  private sensortagid : string =  "B0:B4:48:BD:9D:00"; 
  private barometerGATTService : string = "F000AA40-0451-4000-B000-000000000000"; // service AA40
  private barometerGATTSCharacteristic  : string = "F000AA42-0451-4000-B000-000000000000"; // wake AA42
  private barometerGATTDATA : string = "F000AA41-0451-4000-B000-000000000000" //data AA41
   

searchBluetooth(event) {

this.ble.scan([], 5).subscribe(
device => {

this.bluetoothdevice = ("Found device: " + JSON.stringify(device));

},
err => {
this.bluetoothdevice =("Error occurred during BLE scan: " + JSON.stringify(err));
},
() => {
this.bluetoothdevice =("End of devices...");
}
);





}

connectSensortag(event) {



this.ble.connect(this.sensortagid).subscribe(
connect => {

//this.bluetoothdevice = (JSON.stringify(connect));

},
err => {
this.bluetoothdevice =(JSON.stringify(err));
}
);


  
}

wakeBarometer(event) {

var configData = new Uint8Array(1);
configData[0] = 0x01;
this.ble.write(this.sensortagid,this.barometerGATTService,this.barometerGATTSCharacteristic,configData.buffer).then((callback: string) => 
this.bluetoothdevice = (callback)

)

.catch((error: any) => console.log(error));



}

getBarometerData(event) {
  this.ble.read(this.sensortagid,this.barometerGATTService,this.barometerGATTDATA).then((callback: ArrayBuffer) =>
  
  
  this.bytesToString(callback)
   
   )

.catch((error: any) => console.log(error));



}


bytesToString(buffer) {
    var datahex =  Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
   console.log(datahex); 
    this.bluetoothdevice = ''; 
  this.calctempData(datahex);
  this.calctbaroData(datahex);
    
}
calctempData(rawdata) {
  var temperature = rawdata[5] + rawdata[4] + rawdata[2] + rawdata[3] + rawdata[0] + rawdata[1];
  console.log("Raw Temp : " ,temperature);
  var datatmp = parseInt(temperature,16); 
  
  this.temperature = ((datatmp / 100) + " °C"); 
  console.log((datatmp / 100) + "°C"); 

}

calctbaroData(rawdata) {
  var Baro = rawdata[10] + rawdata[11] + rawdata[8] + rawdata[9] + rawdata[6] + rawdata[7];
  console.log("Raw Baro : " ,Baro);
  var databaro = parseInt(Baro,16); 
  
  this.airPressure = ((databaro / 100) + " hPa"); 
  console.log((databaro / 100) + " hPa"); 

}




  stringToBytes(string) {
   var array = new Uint8Array(string.length);
   for (var i = 0, l = string.length; i < l; i++) {
       array[i] = string.charCodeAt(i);
    }
    return array.buffer;
}


}
